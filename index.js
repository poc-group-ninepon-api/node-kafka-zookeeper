const Kafka = require('node-rdkafka')
const eventType = require('./eventType')
require('dotenv').config()
let count = 1;
const responseToProducer = false

const { connectSetting } = require('./configs/connect')

const consumerGroup = `${process.env.CONSUMER_GROUP}`
const consumer = Kafka.KafkaConsumer({
    'group.id': `${consumerGroup}`,
    'metadata.broker.list':`${process.env.BROKER_LIST}`,
    'partition.assignment.strategy': 'range',
    'enable.auto.commit': false,
    'auto.commit.interval.ms': 5000,
    'event_cb': true,
    // 'rebalance_cb': (err, assignment) => {
    //     if (err.code === Kafka.CODES.ERRORS.ERR__ASSIGN_PARTITIONS) {
    //         console.log(`😱 Kafka.CODES.ERRORS.ERR__ASSIGN_PARTITIONS`);
    //         // Note: this can throw when you are disconnected. Take care and wrap it in
    //         // a try catch if that matters to you
    //         // this.assign(assignment);
    //     } else if (err.code == Kafka.CODES.ERRORS.ERR__REVOKE_PARTITIONS){
    //         console.log(`😱 Kafka.CODES.ERRORS.ERR__REVOKE_PARTITIONS`);
    //         // Same as above
    //         // this.unassign();
    //     } else {
    //         // We had a real error
    //         console.log(`😱 rebalance_cb err`);
    //         console.error(err);
    //     }
    // },
    'offset_commit_cb': (err, topicPartitions) => {
        if (err) {
            // There was an error committing
            console.log(`😱 offset_commit_cb err`);
            console.error(err);
        } else {
            // Commit went through. Let's log the topic partitions
            count = 1;
            console.log(`🤩 SUCCESS reset count`);
            console.log(topicPartitions);
        }
    }
})

var producer = null
if(responseToProducer){
    producer = Kafka.Producer(connectSetting);
    producer.connect();
}

consumer.connect();

consumer
    .on('ready' ,()=>{
        consumer.subscribe([`${process.env.TOPIC}`, 'topica', 'topicb'])
        consumer.consume();
        console.log(`😀 consumer: ${consumerGroup} ready...`)
    })
    .on('data',(data)=>{
        // console.log(`received message (${count}): ${eventType.fromBuffer(data.value)}`)
        // console.log(`received message (${count}): ${data.value.toString()}`);
        // console.log(data.value.toString());
        console.log({
            topic: data.topic,
            offset: data.offset,
            partition: data.partition
        });
        const extractObj = JSON.parse(data.value.toString());
        console.log(extractObj);

        if(responseToProducer){
            producer.produce(
                `${extractObj.node_uniqueue}`,null,
                Buffer.from(`WOW!! thank ${extractObj.node_uniqueue}`),
                `key-ninepon`, Date.now(),
            )
        }

        count++;

        //set finish this job
        try{
            setTimeout(() => {
                consumer.commit();
                console.log(`consumer.commit() is OK!`);
            }, 5000);
        }catch(err){
            console.log(`ERR consumer.commit`);
            console.log(err);
        }
    })
    .on('event', (err, message)=>{
        if(err){
            console.log(`😱 event err`);
            console.error(err);
            console.log(`---------`);
        }else{
            console.log(`🤩 event message`);
            console.log(message);
            console.log(`---------`);
        }
    })